{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": "true",
    "editable": "true",
    "tags": [
     "Frozen"
    ]
   },
   "source": [
    "<header style=\"text-align:center;color:darkgray\">Biology on the Command Line</header>\n",
    "<header style=\"text-align:center;color:darkgray\">2021</header>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": "true",
    "editable": "true",
    "tags": [
     "Frozen"
    ]
   },
   "source": [
    "<h1>Help</h1>\n",
    "\n",
    "<p>This page contains basic information about the course - please read through it before starting. Later, you can use it as a quick reference for problems or links to the course notes for each workspace.</p>\n",
    "\n",
    "<p>The topics covered in this course may seem incredible complex for those unfamiliar with them, and you may suspect that you will forget most of these details once the course is over. This is almost definitely true. However, even just knowing that some of these topics exist in the first place could be a major help if you ever need to pick them up again as you continue your studies - at the very least you will know what search terms to use.</p>\n",
    "\n",
    "<h3>Quick tips</h3>\n",
    "\n",
    "<p>The free Binder JupyterHub server hosting this environment comes with some limits:</p>\n",
    "\n",
    "<ul>\n",
    "\t<li>Each time you launch the course Binder will create a <b>new</b> server. Any previous changes you had made or documents you created will be gone. If you need to save a document (such as a script), you can download it from the server or simply copy-paste the code somewhere else.</li>\n",
    "\t<li>After <b>~30 minutes of inactivity</b> or <b>~7 hours total uptime</b> the server will shut down automatically.</li>\n",
    "\t<li>The server is not very powerful (less so than even an old laptop or desktop), though this will not be an issue for this course.</li>\n",
    "</ul>\n",
    "\n",
    "<p>To open a terminal you can use the Open Terminal button at the top of the notebook (<img style=\"height:18px;vertical-align:-20%\" src=\"jupyterlab-workshop-extension/style/terminal.svg\">), or the shortcut <button style=\"border-style: outset\">SHIFT</button>+<button>TAB</button>.</p>\n",
    "\n",
    "<p>Drag any tab to dock it to the top/bottom/left/right of the screen. You can have multiple tabs open at once inside JupyterLab.</p>\n",
    "\n",
    "<p>Some notes (such as the Contents and Help pages) are Jupyter Notebooks. These will not be discussed in detail, but double-clicking paragraphs in these notes will convert them to Edit Mode. If you suspect this has happened, use <button>CTRL</button>+<button>ENTER</button> to reset it. You can prevent this behaviour entirely using the Lock Cells button at the top of the notebook (<img style=\"height:18px;vertical-align:-15%;opacity:0.75\" src=\"jupyterlab-workshop-extension/style/lock.svg\">). This change will reset if a notebook is closed/reopened.</p>\n",
    "\n",
    "<p>Some URL links may not open directly when clicked. Use <i>Right click -> Open link in new tab</i> to view them on a separate page.</p>\n",
    "\n",
    "<p>To search within a document use the browser search function (e.g. in Chrome, click the three dots and \"Find\"). The <button>CTRL</button>+<button>F</button> hotkey will not work.</p>\n",
    "\n",
    "<p>You can download the course notes from the JupyterLab file browser (<span class=\"path\"><i>DayX_course-notes.html</i></span>) and view them in any internet browser.</p>\n",
    "\n",
    "<h2>Links to course notes</h2>\n",
    "\n",
    "[Day 1](./Day1_course-notes.html) | [Day 2](./Day2_course-notes.html) | [Day 3](./Day3_course-notes.html) | [Day 4](./Day4_course-notes.html)  | [Day 5](./Day5_course-notes.html)\n",
    "\n",
    "<h2>General Information</h2>\n",
    "\n",
    "<h3><i>Binder/JupyterLab</i></h3>\n",
    "\n",
    "<p><b>Workspaces layout</b>: Try have only one JupyterLab workspace open at once in your browser. If you have the same workspace open in multiple tabs the layout will not be maintained (i.e. this help file and the course notes docked to the top/bottom of the screen).</p>\n",
    "\n",
    "<p><b>File browser</b>: You can open the JupyterLab File Browser from the folder icon on the left sidebar or shortcut <button>CTRL</button>+<button>SHIFT</button>+<button>F</button> to access workshop files (such as course notes) through a graphical interface.</p>\n",
    "\n",
    "<p><b>Edit Mode</b>: Press <button>CTRL</button>+<button>ENTER</button> in a cell to change it back to the normal view if you accidentally double-click it and enter Edit Mode. If you do this often you can also simply lock all cells in the notebook using the Lock Cells button at the top of the notebook (<img style=\"height:18px;vertical-align:-15%;opacity:0.75\" src=\"jupyterlab-workshop-extension/style/lock.svg\">).</p>\n",
    "\n",
    "<p><b>Relaunching the terminal</b>: If you accidentally close the terminal tab you can restore it from the \"Running Kernels and Terminals\" menu on the left of the screen (black circle containing a white square). To open a new terminal you can use the Open Terminal button at the top of the notebook (<img style=\"height:18px;vertical-align:-20%\" src=\"jupyterlab-workshop-extension/style/terminal.svg\">), or the shortcut <button>SHIFT</button>+<button>TAB</button>.</p>\n",
    "\n",
    "<p><b>JupyterLab tabs and docking</b>: JupyterLab can have multiple internal tabs open at once and you can dock up to four tabs to the top/bottom/left/right of the screen simultaneously. This is useful if you want to have two or more separate documents open at once (or multiple documents and/or terminals).</p>\n",
    "\n",
    "<h3><i>Errors</i></h3>\n",
    "\n",
    "<p><b>\"Failed to connect to event stream\"</b>: This error can occur when launching the Binder server. Simply refresh the page (F5) and the Binder launch will have continued in the background.</p>\n",
    "\n",
    "<p><b>\"File is different to one on disk\"</b>: This warning may occasionally occur if more than one tab is open at once. You can select either \"revert\" or \"overwrite\", as any changes will only last as long as the server instance in which you are working. Closing the Help file will also prevent this from occuring.</p>\n",
    "\n",
    "<p><b>\"Directory not found\"</b>: This error means the server has automatically shut down. Make sure to regularly back-up any data you wish to keep (such as scripts) to your personal computer, as changes from previous sessions will not be saved. However, the course is structured such that this should not be necessary very often.</p>\n",
    "\n",
    "<h3><i>Reading the course notes</i></h3>\n",
    "\n",
    "<p><i>It is possible that the course notes may look slightly different across browsers. If you see anything strange just ask.</i></p>\n",
    "\n",
    "<p><b>Font size:</b> You can adjust the browser zoom using the <button>CTRL</button>+<button>+</button> and <button>CTRL</button>+<button>-</button> shortcuts, in case the font is too small or large. You can also adjust the JupyterLab content or UI font size specifically from the <b>Settings -> JupyterLab Theme -> Increase/Decrease</b> relevant font size options.</p>\n",
    "\n",
    "<p><b>Commands</b>: Commands will usually be formatted as the following: <code>command [input 1] [input 2] ...</code>. This means you should type \"command\" into the terminal, followed by an additional one or more user-supplied inputs separated by spaces (the <code>...</code> indicates that more arguments could be added but are not shown). Square brackets and ellipses should not be included in the typed command.</p>\n",
    "\n",
    "<p>Lines starting with a \"<span class=\"centered\" style=\"font-size:20px\">⮚</span>\" symbol indicate that what follows is a command that you should run in the terminal.</p>\n",
    "\n",
    "<p><b>Keyboard shortcuts</b>: Shortcuts (and key press combinations) will be displayed using a key symbol. E.g. <button>ENTER</button> means \"press the enter key\".</p>\n",
    "\n",
    "<p><b>Files, directories and paths</b> will be in blue: <span class=\"path\">file.txt</span>, <span class=\"path\">directory</span>, <span class=\"path\">/home/user/</span></p>\n",
    "\n",
    "<p><b>Blue, orange and green boxes</b> can be expanded by clicking the box. Orange boxes contain additional information related to the workshop contents and blue boxes contain the answers to posed questions or excercises. Green boxes contain \"advanced\" information or exercises (relative to the current section of work) - if they are too complex, you can always return to them at the end of the day or on another day.</p>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"details-container\">\n",
    "<details>\n",
    "<summary class=\"info\">Click here to reveal additional information.</summary>\n",
    "<div class=\"answer info\">\n",
    "\t<span>Additional information.<i/></span><br>\n",
    "</div>\n",
    "</details>\n",
    "</div>\n",
    "\n",
    "\n",
    "<div class=\"details-container\">\n",
    "<details>\n",
    "<summary class=\"\">This is a question or task.</summary>\n",
    "<div class=\"answer\">\n",
    "\t<span>Answer or explanation.</span><br>\n",
    "</div>\n",
    "</details>\n",
    "</div>\n",
    "\n",
    "<div class=\"details-container\">\n",
    "<details>\n",
    "<summary class=\"advanced\">This is advanced information or an advanced exercise.</summary>\n",
    "<div class=\"answer advanced\">\n",
    "\t<span>More advanced information about the current topic.</span><br>\n",
    "</div>\n",
    "</details>\n",
    "</div>"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
